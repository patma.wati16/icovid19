import "./script/component/app-bar.js";
import "./script/component/headerJumbo.js";
import "./script/component/app-foot.js";

import 'regenerator-runtime';
import "./styles/style.css";
import '@fortawesome/fontawesome-free';
import '@fortawesome/fontawesome-svg-core';

import "jquery";
import "popper.js";
import "bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';


import "./script/component/breadcrumb-detail.js";
import "./script/component/detail-header.js";


import main from "./script/view/main.js";


document.addEventListener("DOMContentLoaded", main);