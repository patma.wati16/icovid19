import './provinsi-item.js';
import $ from "jquery";

class ProvinsiList extends HTMLElement {
    set provinsi(provinsi) {
        this._provinsi = provinsi;
        this.render();
    }

    render() {
        this.innerHTML = "";
        this._provinsi.map(data => {
            const provinsiItemElement = document.createElement("provinsi-item");
            provinsiItemElement.provitem = data;
            this.appendChild(provinsiItemElement);
        })
    }

    renderError(message) {
        this.innerHTML = "";
        this.innerHTML += `<h2 class="placeholder">${message}</h2>`;
    }
}

customElements.define("provinsi-list", ProvinsiList);