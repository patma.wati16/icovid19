import $ from "jquery";
class SearchBar extends HTMLElement {
    connectedCallback(){
        this.render();
    }

    set clickEvent(event) {
        this._clickEvent = event;
        this.render();
    }


    get value() {
        return this.querySelector("#searchElement").value;
    }

    render() {
        this.innerHTML = `
        <style>
        search-bar {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
                -ms-flex-pack: center;
                    justify-content: center;
            margin: 2rem 0;
          }
          
        search-bar .search {
            width: 500px;
          }
          
        search-bar .search__form {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            gap: 10px;
          }
          
        search-bar .search__input {
            width: 100%;
            padding: 0.5rem 1rem;
            border: none;
            border-bottom: orange solid 1px;
            font-size: 1.2rem;
            color: #823c07;
            -webkit-transition: 0.2s;
            transition: 0.2s;
          }
          
        search-bar .search__input:focus {
            outline: none;
            border-bottom: #ff9900 solid 2px;
          }
         
        search-bar .search__btn {
            padding: 0.7rem 1.2rem;
            background: orange;
            -webkit-box-shadow: 10px 11px 17px -7px rgba(199, 137, 43, 0.75);
                    box-shadow: 10px 11px 17px -7px rgba(199, 137, 43, 0.75);
            border: none;
            color: #fff;
            font-weight: 700;
            border-radius: 5px;
            cursor: pointer;
            font-size: 1rem;
            letter-spacing: 2px;
            -webkit-transition: ease-in-out 0.2s;
            transition: ease-in-out 0.2s;
          }
          
          @media only screen and (max-width: 1024px) {
            search-bar .search__input {
              padding: 0.3rem 1.2rem;
            }
            search-bar .search__btn {
              font-size: 0.8rem;
            }
          }
          
          @media only screen and (max-width: 1024px) {
            search-bar .search__input {
              padding: 0.1rem 1rem;
            }
            search-bar .search__btn {
              font-size: 0.8rem;
            }
          }
          
        </style>
       <div id="search-container" class="search container-fluid">
        <div class="search__form">
           <input placeholder="Search Provinsi" id="searchElement" type="search" class="search__input">
           <button id="searchButtonElement" type="submit" class="search__btn">
           <i class="fas fa-search">Search</i></button>
        </div>
       </div>
       `;

        this.querySelector("#searchButtonElement").addEventListener("click", this._clickEvent);
    }
}

customElements.define("search-bar", SearchBar);
