import ImageHero from "../assets/gambarawalan.png";
class HeaderJumbo extends HTMLElement {
    connectedCallback(){
        this.render();

    }

    render() {
        this.innerHTML = `
        <style>
        header-nav .text_header {
            text-transform: capitalize;
            color:white;
            font-size: 4.6rem;
            font-family: 'Zilla Slab Highlight', cursive;
            margin-top: 1.5rem;
        
          }
        header-nav .text_desc{
            font-family: 'Caveat', cursive;
            text-transform: capitalize;
            color:white;
            font-size: 2.0rem;
        }
        header-nav img {
            width:900px;
            display: -webkit-box;
            display: -ms-flexbox;
            -webkit-box-pack: center;
                -ms-flex-pack: center;
                    justify-content: center;
        }
        @media only screen and (max-width: 1024px) {
            header-nav .text_header {
                font-size: 3.0rem;
                margin-top: 0.5rem;
            
              }
            header-nav .text_desc{
                font-size: 1.5rem;
            }
              
        </style>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-6">  
                    <img src="${ImageHero}" class="img-fluid">
                </div>
                <div class="col-12 col-md-6">
                    <p class="text_header text-justify">iCOVID-19 Information Case</p>
                    <p class="text_desc">Update penyebaran kasus covid 19 di Indonesia</p>
                </div>
            </div>
        </div>`;
    }
}

customElements.define("header-nav", HeaderJumbo);
