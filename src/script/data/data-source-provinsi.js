
class DataSourceProvinsi {
    static TampilProvinsi() {
        return fetch(`http://apicovid19indonesia-v2.vercel.app/api/indonesia/provinsi/more`)
        .then(response => {
            return response.json();
        })
        .then(responseJson => {
//            const filterProvinsi = responseJson.filter(data => data.provinsi.toUpperCase().includes(keyword.toUpperCase()))
            const filterProvinsi = responseJson.filter(data => data.kasus > 150000)

            if(responseJson) {
               return Promise.resolve(filterProvinsi);
            } else {
                return Promise.reject(`${keyword} is not found`);
            }
        })
    }

    static TampilIndonesia(){
        return fetch(`http://apicovid19indonesia-v2.vercel.app/api/indonesia/more`)
        //http://apicovid19indonesia-v2.vercel.app/api/indonesia/more
        .then(response =>{
            return response.json();
        }).then(responseJSON => {
            if(responseJSON){
                return Promise.resolve(responseJSON);
            }else{
                return Promise.reject('Not FOUND DATA')
            }
        })
    }

    static DetailProvinsi(){
        return fetch(`http://apicovid19indonesia-v2.vercel.app/api/indonesia/provinsi/more`)
        .then(response =>{
            return response.json();
        }).then(responseJSON => {
            if(responseJSON){
                return Promise.resolve(responseJSON);
            }else{
                return Promise.reject('Not FOUND DATA')
            }
        })
    }
}


 
 export default DataSourceProvinsi;