import './indo-item.js';

class IndoList extends HTMLElement {
  set detailindo(detailindo) {
      this._detailindo = detailindo;
      this.render();
    }

  render() { 
    this.innerHTML = "";
    const indoItemElement = document.createElement("indo-item");   
    indoItemElement.provdp=this._detailindo;
    this.appendChild(indoItemElement);
  }

  renderError(message) {
    this.innerHTML = "";
    this.innerHTML += `<h2 class="placeholder">${message}</h2>`;
    }
}

customElements.define("indo-list", IndoList);