import $ from "jquery";
import DataSource from '../data/data-source.js';
import "./breadcrumb-detail.js";

class ProvinsiItem extends HTMLElement {

    set provitem(provitem) {
        this._provitem = provitem;
        this.render();
    }

    connectedCallback(){
        this.render();
    }
    myn() {
        var y = document.getElementById("provinsilist")
        var x = document.getElementById("detailprovinsi")
            if (x.style.display === "none") {
                x.style.display = "block";
                y.style.display = "none";
            } else {
                x.style.display = "none";
                y.style.display ="block";
            }
        }
    render() {
        this.innerHTML = `
           <div class="list-info" >
               <h2 class="title">${this._provitem.provinsi}</h2>
            </div>
            <div class="detail-info">
               <p><i class="fa fa-medkit fa-lg"></i>Kasus :${this._provitem.kasus.toLocaleString()}</p>
               <p><i class="fa fa-heartbeat fa-lg"></i>Sembuh:${this._provitem.sembuh.toLocaleString()}</p>
               <p><i class="fa fa-stethoscope fa-lg"></i>Dirawat:${this._provitem.dirawat.toLocaleString()}</p>
               <p><i class="fa fa-ambulance fa-lg"></i>Meninggal:${this._provitem.meninggal.toLocaleString()}</p> 
            </div>
            <button class="btn btn-outline-warning rounded-pill float-right btn-sm btnView" data-id="${this._provitem.provinsi}" style="display: block;">
                Details <i class="fa fa-arrow-right"></i>
            </button>`;
                    
        // Button on Click
        const detailData = this.querySelector(".btnView");
        detailData.addEventListener("click", function () {
            const idDetail = this.getAttribute("data-id");
            informationProvinsi(idDetail);  
        });
        
        const informationProvinsi = async (idDetail) => {
          try {
                const result = await DataSource.InformasiProvinsi(idDetail);
                renderResult(result);
                this.myn();
            } catch (message) {
                fallbackResult(message)
            }
        }

        // Callback Success
        const renderResult = results => {
            results.map(data=>{
               
            const breadcrumb = document.querySelector("breadcrumb-detail");
            breadcrumb.currentPage = data.provinsi;
            
            const detailHeaderElement = document.querySelector("detail-header");    
            const dataHeader = {
                'provinsi': (data.provinsi ? data.provinsi : 'NaN'),
                'kasus': (data.kasus ? data.kasus : 'NaN'),
                'dirawat': (data.dirawat ? data.dirawat : 'NaN'),
                'sembuh': (data.sembuh ? data.sembuh : 'NaN'),
                'meninggal': (data.meninggal ? data.meninggal : 'NaN'),
            }
            
            detailHeaderElement.update = dataHeader;

            //table detail kelompok umur
            const bj = data.kelompok_umur;
                for (let index in Object.create(bj)){
                    $(`<th>${index}</th>`).appendTo('.headerU')
              }
                for(let vals of Object.values(bj)){
                    $(`<td>${vals.toLocaleString()}</td>`).appendTo('.isiU')
             }

            //table detail jenis kelamin
            const bjo = data.jenis_kelamin;
                for (let index in Object.create(bjo)){
                   $(`<th>${index}</th>`).appendTo('.headerJK')
                }
                for(let vals of Object.values(bjo)){
                    $(`<td>${vals.toLocaleString()}</td>`).appendTo('.isiJK')
                }

            //tabel detail penambahan
            const bja = data.penambahan;
                for (let index in Object.create(bja)){
                   $(`<th>${index}</th>`).appendTo('.headerP')
               }
               for(let vals of Object.values(bja)){
                    $(`<td>${vals.toLocaleString()}</td>`).appendTo('.isiP')
                 }

           });

        };

        // Callback Failed
        const fallbackResult = message => {
            alert(message);
        };
    }
}

customElements.define("provinsi-item", ProvinsiItem);
