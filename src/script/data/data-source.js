class DataSource {
    static searchProvinsi(keyword) {
        return fetch(`http://apicovid19indonesia-v2.vercel.app/api/indonesia/provinsi/more`)
        .then(response => {
            return response.json();
        })
        .then(responseJson => {
            const filterProvinsi = responseJson.filter(data => data.provinsi.toUpperCase().includes(keyword.toUpperCase()))
            if(responseJson) {
                return Promise.resolve(filterProvinsi);
            } else {
                return Promise.reject(`${keyword} is not found`);
            }
        })
    }

    static InformasiProvinsi(id) {
        return fetch(`http://apicovid19indonesia-v2.vercel.app/api/indonesia/provinsi/more`)
        .then(response => {
            return response.json();
        })
        .then(responseJson => {
            const filterdata = responseJson.filter(data => data.provinsi.toUpperCase().includes(id.toUpperCase()))
            if(responseJson) {
                return Promise.resolve(filterdata);
            } else {
                return Promise.reject(`${id} is not found`);
            }
        })
    }
}
 export default DataSource;