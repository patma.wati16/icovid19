class AppFoot extends HTMLElement {

    constructor() {
        super();
        this.shadowDOM = this.attachShadow({
            mode: "open"
        });
    }

    connectedCallback() {
        this.render();
    }

    render() {
        this.shadowDOM.innerHTML = `
    <style>
        @import "./node_modules/font-awesome/css/font-awesome.css";
        .app-foot {
            font-size: 14px;
            color: #525355;
            background-color: black;
            padding : 10px;
            text-align: center;
            display: block;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            color:white;
        } 
        .app-foot .card a {
            text-decoration: none;
        }
        
        .app-foot .card a:visited {
            color: #525355;
        }
        

</style>        
        <div class="app-foot">
        <hr>
            <div class="card d-inline-block w-100 mb-3">
                <span><i class="fa fa-copyright"></i> Indosat Camp | Dicoding Submission 2021 | patmawati | ICovid19</span>
            </div>
        </div>`;
    }
}

customElements.define("app-foot", AppFoot);