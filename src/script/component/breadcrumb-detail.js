import $ from "jquery";
class BreadcrumbDetail extends HTMLElement {

    set currentPage(value) {
        this.setAttribute('currentpage', value);
        this.render();
    }

    render() {
        this.currentpage = this.getAttribute("currentpage") || null;
        this.innerHTML = `
        <style>
        .breadcrumb{
            padding : 5px;
            font-weight : bolder;
            color :blue;
        }
        </style>
        <div class="container-fluid">
            <div class="row justify-content-center">
                 <div class="col-sm-12" >
                    <nav class="d-flow-root" aria-label="breadcrumb">
                        <ol class="breadcrumb float-left">
                            <li class="breadcrumb-item"><a href="#">PROVINSI</a></li>
                            <li class="breadcrumb-item active" aria-current="page">${this.currentpage}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>`;
    }

    connectedCallback() {
        this.render();
    }
}

customElements.define("breadcrumb-detail", BreadcrumbDetail);