import $ from "jquery";

class IndoItem extends HTMLElement {
    set provdp(provdp) {
        this._provdp = provdp;
        this.render();
    }

    connectedCallback(){
        this.render();   
    }
      
  render() {
    this.innerHTML = `
    <style>
    indo-list {
        max-width: 100%;
        margin: 1rem auto 1rem auto;
    }
    
    indo-list > .placeholder {
        font-weight: lighter;
        color: rgba(0,0,0,0.5);
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    
    indo-item {
      background: #fff;
      padding: 20px;
      border-radius: 10px;
      -webkit-transition: all ease-out 0.3s;
      transition: all ease-out 0.3s;
    }
    
    
    indo-item h5{
        font-family:Impact, Haettenschweiler, 'Arial Narrow Bold', sans-serif;
    
    }
    indo-item table{
        font-family: 'Roboto', sans-serif;
        color: black;
        -webkit-transition: all ease-out 0.2s;
        transition: all ease-out 0.2s;
        cursor: pointer;
        text-align: center;
        font-weight: 900;
        padding: 50px;
    }
    
    indo-item table thead{
        font-weight:bolder;
        font-size: 1.0rem;
       
    }    
    indo-item table tbody{
        font-weight:lighter;
        font-size: 0.9rem;       
    }  
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6" >
            <h5><p class="text-center">PENAMBAHAN</p></h5>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr class="table-dark">
                            <td>POSITFI</td>
                            <td>DIRAWAT</td>
                            <td>SEMBUH</td>
                            <td>MENINGGAL</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${this._provdp.penambahan.positif.toLocaleString()}</td>
                            <td>${this._provdp.penambahan.dirawat.toLocaleString()}</td>
                            <td>${this._provdp.penambahan.sembuh.toLocaleString()}</td>
                            <td>${this._provdp.penambahan.meninggal.toLocaleString()}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-sm-6" >
            <h5><p class="text-center">KETERANGAN</p></h5>
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr class="table-dark">
                            <td>ODP</td>
                            <td>PDP</td>
                            <td>SPESIMEN</td>
                            <td>SPESIMEN NEGATIF</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${this._provdp.data.odp}</td>
                            <td>${this._provdp.data.pdp.toLocaleString()}</td>
                            <td>${this._provdp.data.total_spesimen.toLocaleString()}</td>
                            <td>${this._provdp.data.total_spesimen_negatif.toLocaleString()}</td>
                        </tr>
                    <tbody>
                </table>
            </div>
        </div>
    </div>`;
            
  }

}

customElements.define("indo-item", IndoItem);