import $ from "jquery";
class DetailHeader extends HTMLElement {

  set update(value) {
    this.setAttribute('provinsi', value.provinsi);
    this.setAttribute('kasus', value.kasus);
    this.setAttribute('dirawat', value.dirawat);
    this.setAttribute('meninggal', value.meninggal);
    this.setAttribute('sembuh', value.sembuh);
    this.render();
  }

  render() {
    this.provinsi = this.getAttribute("provinsi") || null;
    this.kasus = this.getAttribute("kasus") || null;
    this.dirawat =this.getAttribute("dirawat")||null;
    this.sembuh   = this.getAttribute("sembuh") || null;
    this.meninggal = this.getAttribute("meninggal") || null;
    this.innerHTML = `
    <div class="container-fluid detail-item">
      <div class="row justify-content-center">
        <div class="col-sm-6 table_content" >
        <h5><p class="text-center">DETAIL INFORMATION</p></h5>
          <table class="table table-striped">
            <thead>
              <tr>
                <td>PROVINSI</td>
                <td>KASUS</td>
                <td>DIRAWAT</td>
                <td>SEMBUH</td>
                <td>MENINGGAL</td>
              </tr>
            </thead>
            <tbody>
              <tr>             
                <td>${this.provinsi}</td>
                <td>${this.kasus}</td>
                <td>${this.dirawat}</td>
                <td>${this.sembuh}</td>
                <td>${this.meninggal}</td>
              </tr>
            </tbody>
          </table>
        </div>
        
        <div class="col-sm-6 table_content" >
        <h5><p class="text-center">JENIS KELAMIN</p></h5>
            <table class="table table-striped">
              <thead>
                <tr class="headerJK">
                  <td>Jenis Kelamin</td>
                </tr>
              </thead>
              <tbody>
                <tr class="isiJK">             
                  <td>Jumlah</td>
                </tr>
              </tbody>
            </table>
        </div>

        <div class="col-sm-6 table_content" >
        <h5><p class="text-center">KELOMPOK UMUR</p></h5>
            <table class="table table-striped">
              <thead>
                <tr class="headerU">
                  <td>Kelompok Umur</td>
                </tr>
              </thead>
              <tbody>
                <tr class="isiU">
                  <td>Jumlah</td>
                </tr>
              </tbody>
            </table>
       </div>

      <div class="col-sm-6 table_content" >
        <h5><p class="text-center">DETAIL PENAMBAHAN</p></h5>
            <table class="table table-striped">
              <thead>
                <tr class="headerP">
                  <td>Keterangan</td>
                </tr>
              </thead>
              <tbody>
                <tr class="isiP">
                  <td>Jumlah</td>
                </tr>
              </tbody>
            </table>
       </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <button id="backTo" class="btn btn-outline-warning rounded-pill float-right btnBack">
          <i class="fa fa-arrow-left"></i> Back
        </button>
      </div>
    </div>
  </div>`;

  $('.btnBack').click(function() {
    $('#provinsilist').show();
    $('#detailprovinsi').hide();

    });
        
  }

  connectedCallback() {
    this.render();
  }
}

customElements.define("detail-header", DetailHeader);
