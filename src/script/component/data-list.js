import './data-item.js';


class DataList extends HTMLElement {
    set ddlist(ddlist) {
        this._ddlist = ddlist;
        this.render();
    }
    render() {
        this.innerHTML = "";
            const dataItem1 = document.createElement("data-item");
            dataItem1.style.backgroundColor="blue";
            const dataItem2 = document.createElement("data-item");
            dataItem2.style.backgroundColor ="green";
            const dataItem3 = document.createElement("data-item");
            dataItem3.style.backgroundColor="orange";
            const dataItem4 = document.createElement("data-item");
            dataItem4.style.backgroundColor="red";


            dataItem1.indopositif = this._ddlist;
            dataItem2.indosembuh = this._ddlist;
            dataItem3.indorawat = this._ddlist;
            dataItem4.indodead = this._ddlist;
            
            this.appendChild(dataItem1);
            this.appendChild(dataItem2);
            this.appendChild(dataItem3);
            this.appendChild(dataItem4);
     
      
    }

    renderError(message) {
        this.innerHTML = "";
        this.innerHTML += `<h2 class="placeholder">${message}</h2>`;
    }
}

customElements.define("data-list", DataList);