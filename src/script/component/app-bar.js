class AppBar extends HTMLElement {
    connectedCallback(){
        this.render();
    }

    render() {
        this.innerHTML = `
        <style>
        @import url('https://fonts.googleapis.com/css2?family=Merienda+One&family=Rancho&display=swap');
        app-bar {
            display: block;
            padding: 10px;
            width: 100%;
            background-color:black;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        }
        app-bar span {
            font-weight:400;
            color: white;
            font-size: 2.0rem;
            font-family: 'Merienda One', cursive;
            border : solid 1px black;
        }
        app-bar ul li a {
            color:white;
            font-size : 1.0rem;
        }
        app-bar ul li a:hover{
            font-weight :500;
            color:white;
        }
        </style>
        <div clas="container-fluid">
            <nav class="nav float-left">
                <span >ICOVID19</span>
            </nav>
                <ul class="nav justify-content-end">
                    <li class="nav-item">
                        <a class="nav-link active" href="">Home</a>
                    </li>
                    <li class ="nav-item"> 
                        <a class="nav-link active" href="#footerid">About</a>
                    </li>
                </ul>
        </div>`;
    }
}

customElements.define("app-bar", AppBar);