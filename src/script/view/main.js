import '../component/provinsi-list.js';
import '../component/data-list.js';
import '../component/search-bar.js';
import '../component/provinsi-item.js';
import '../component/indo-list.js';
import '../component/indo-item.js';
import $ from "jquery";



import DataSource from '../data/data-source.js';
import DataSourceProvinsi from '../data/data-source-provinsi.js';
const main = () => {
    const searchElement = document.querySelector("search-bar");

    const provinsiListElement = document.querySelector("provinsi-list");
    const DataListElement = document.querySelector("data-list");

    const detailIND  = document.getElementById("detailIND");
    const detailListElement = document.querySelector("indo-list");
    


    const onButtonSearchClicked = () => {
     if(searchElement.value==""){
         TampilAllProvinsi();
     }else{
        searchProvinsi(searchElement.value); 
     }
        
    };
    //search provinsi
    const searchProvinsi = async (keyword) => {
        try {
            const result = await DataSource.searchProvinsi(keyword);
            renderResult(result);
        } catch (message) {
            fallbackResult(message)
        }
    }

    //Tampilan Data All Provinsi
    const TampilAllProvinsi = async () => {
        try {
            const result = await DataSourceProvinsi.TampilProvinsi();
            renderResult(result);
        } catch (message) {
            fallbackResult(message)
            
        }
    }

    const renderResult = results => {
        provinsiListElement.provinsi = results;
    };

    const fallbackResult = message => {
        provinsiListElement.renderError(message);
    };

    //Tampilan Data Kasus di Indonesia
    const TampilDataIndonesia = async()=>{
        try {
            const result= await DataSourceProvinsi.TampilIndonesia();
            renderResultIndonesia(result)
        } catch (message) {
            fallbackResulIndonesia(message)
        }
    }

    const renderResultIndonesia = results => {
        DataListElement.ddlist =results;
    };
    const fallbackResulIndonesia = message =>{
        DataListElement.renderError(message)
    }


    const TampilDetailIndonesia = async()=>{
        try {
            const result= await DataSourceProvinsi.TampilIndonesia();
            renderDetailIndonesia(result)
          // alert(JSON.stringify(result));
        } catch (message) {
            fallbackDetailIndonesia(message)
        }
    }
    const renderDetailIndonesia = results =>{
        detailListElement.detailindo = (results);
    }
    const fallbackDetailIndonesia = message => {
        detailListElement.renderError(message);
    }

    //more information kasus Indonsia
    detailIND.addEventListener("click", function () {
       $("#panelinfo").slideToggle("slow");
    
    });

    // Default Tampilan
    TampilAllProvinsi();
    TampilDataIndonesia();
    TampilDetailIndonesia();
    
    // Search Data
    searchElement.clickEvent = onButtonSearchClicked;
 
};

export default main;