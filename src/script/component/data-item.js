import $ from "jquery";
class DataItem extends HTMLElement {
    set indopositif(indopositif) {
        this._indopositif = indopositif;
        this.render_positif();
    }
    
    set indosembuh(indosembuh){
        this._indosembuh = indosembuh;
        this.render_sembuh();
    }
    
    set indorawat(indorawat){
        this._indorawat = indorawat;
        this.render_rawat();
    }

    set indodead(indodead){
        this._indodead = indodead;
        this.render_dead();
    }

    render_positif() {
        this.innerHTML = `
        <div class="list-info">
            <h2 class="title-data">Positif</h2>
                <div class="detail-data">
                    <p>${this._indopositif.total.positif.toLocaleString()}</p>
                </div>
        </div>`;
    }

    render_sembuh(){
        this.innerHTML =`
        <div class="list-info">
            <h2 class="title-data">Sembuh</h2>
                <div class="detail-data">
                    <p>${this._indosembuh.total.sembuh.toLocaleString()}</p>
                </div>
        </div>`;
        
    }
    render_rawat(){
        this.innerHTML =`
        <div class="list-info">
            <h2 class="title-data">Dirawat</h2>
                <div class="detail-data">
                    <p>${this._indorawat.total.dirawat.toLocaleString()}</p>
                </div>
        </div>`;
        
    }
    render_dead(){
        this.innerHTML =`
        <div class="list-info">
            <h2 class="title-data">Meninggal</h2>
                <div class="detail-data">
                    <p>${this._indodead.total.meninggal.toLocaleString()}</p>
                </div>
        </div>`;
        
    }

}

customElements.define("data-item", DataItem);